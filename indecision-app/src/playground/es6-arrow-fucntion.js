const square = function (x) {
    return x * x;
};

console.log(square(9));

const squareArrow = (x) => {
    return x * x;
};

const squareArrowTwo = (x) => x * x;

console.log(squareArrow(5));
console.log(squareArrowTwo(4));


const getFirstName = (fullName) => {
    return fullName.split(' ')[0];
};

console.log(getFirstName('Paulo Filho'));

const getFirstNameTwo = (fullName) => fullName.split(' ')[0];

console.log(getFirstNameTwo('Amina Cahu'));
