const person = {
  name: 'Paulo',
  age: 32,
  location: {
    city: 'Oporto',
    temp: 21
  }
}

const { name: firstName = 'Anonymous', age } = person;

console.log(`${firstName} a ${age} ans.`);

const { city, temp: temperature } = person.location
if (city && temperature){
  console.log(`C'est ${temperature}°C à ${city}.`);
}

const book = {
  title: 'Les Fleurs du Mal',
  author: 'Charles Baudelaire',
  publisher: {
    name: 'Folio Classique'
  }
}

const { title, author } = book
const { name: publisherName = 'Self-Published' } = book.publisher;
console.log(`Titre: ${title} Auteur: ${author} Éditeur: ${publisherName}`);

const address = ['58 Rue Jean Bleuzan', 'Vanves', 'Île-de-France', '92170' ];
const [ , city2, state ] = address;
console.log(`Vous êtes à ${city2}, ${state}.`)

const item = ['Americano', '2€', '2,50€',  '2,75€'];
const [ itemName, , , mediumSize ] = item;
console.log(`Un ${itemName} moyen coûte ${mediumSize}`)
