const isAdult = (age) => age >= 18;
const canDrink = (age) => age >= 18;
const isSenionCitizen =  (age) => age >= 65;

export { isAdult, canDrink, isSenionCitizen as default };
